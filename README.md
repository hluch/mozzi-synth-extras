# HLUCH - Mozzi Synth extras

#### Here you can find extra files and information about the HLUCH Mozzi Synth.
Code is in the [mozzi-synth](https://gitlab.fel.cvut.cz/hluch/mozzi-synth) repository.

## What is the Mozzi synth?
The Mozzi Synth is a mono digital programmable synthesizer.
It has only 4 potentiometers and 1 audio output but can be easily expanded with more accessories.
The main microprocessor is Arduino Nano.

This synth is meant to teach you the basics of the [Mozzi library](https://sensorium.github.io/Mozzi/).
You can learn to code with the library by going through simple code examples.
After you learn the basics of Mozzi, you can experiment and make your own synthesizer.
You can make a sampler, an AM/FM synth, glitch machine or whatever else, that makes sound.


## What do you need to make it?
-	 __1x__ __Arduino Nano__ with soldered headers
-	 __4x__ linear 100k panel mount __potentiometers__
-	 __1x__	__stereo 3.5mm jack socket__ _(the synth produces mono sound, but by using a mono jack, you won't be able to use both headphone speakers)._
-	 __1x__ __270 Ohm resistor__
-	 __1x__ __100 nF capacitor__
-	 __4x__ __dupont female-female wire__
-	 __6x__ at least 45mm long __wire__
-	 __1x__ 3D printed __Main Case__


## Tools
- soldering iron, solder
- wire cutters
- cable stripper / strong teeth

## Extras
-	 1x USB A to USB Micro B cable
-	 1x wired headphones / speakers
***

## Build instructions

![Connection Diagram](/assets/schematic.png)
#### Step 1: Soldering potentiometers
First of all mount the potentiometers on the top side of the case. This will make it easier to solder.
Split 3 dupont female-to-female wires in half.
Now solder the wires. All middle pins will have a dupont wire connected to it. All left pins need to be connected together, all right pins need to be connected together.
One dupont needs to be solder to a left pin of the bottom left potentiometer (looking at the top of the case) and one last dupont connected to the right pin to the same potentiometer.
#### Step 2: Soldering the jack
Bend the legs of the switch in the jack, which are not needed.
Solder the RC filter.
Split a dupont wire in half and solder one wire to the ground pin of the jack and other wire to both audio channels.
#### Step 3: Mount Arduino
From the top of the case, start putting in the Arduino board. Firstly push in the USB connecter, then carefully push on the other side of the board until the board clicks in.
#### Step 4: Mount and connect the jack
Mount the jack, now connect the ground dupont wire to GND (which is next to the RST and D2 pins).
Connect the signal wire to D9.
#### Step 5: Mount and connect potentiometers
Mount the potentiometers, connect the +5V wire to 5V, connect the GND wire to GND.
Connect potentiometer 1 (top left) to A0, potentiometer 2 (top right) to A1, potentiometer 3 (bottom left) to A2 and potentiometer 4 (bottom right) to A3.
#### Step 6: Test audio and potentiometers
An installation guide can be found in the [mozzi-synth](https://gitlab.fel.cvut.cz/hluch/mozzi-synth) repository.
Upload the tests and check if everything is functioning as intended.
#### Step 7: Have fun with programming
Not much I can add...
Good luck and don't be afraid to break expectations of what is "supposed" to be music.
***


## Useful links
[Hluch Website](http://hluch.fel.cvut.cz/)

[Hluch GitLab Repository](https://gitlab.fel.cvut.cz/hluch)

[mozzi-synth Repository](https://gitlab.fel.cvut.cz/hluch/mozzi-synth)

[Mozzi Main Page](https://sensorium.github.io/Mozzi/)

[Mozzi Download](https://github.com/sensorium/Mozzi)

[Mozzi Documentation](https://sensorium.github.io/Mozzi/doc/html/index.html)
